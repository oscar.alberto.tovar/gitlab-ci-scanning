package shck

import (
	"fmt"
	"io"
	"path/filepath"

	"mvdan.cc/sh/v3/syntax"
)

type Analyzer struct {
	Name string
	Doc  string
	Run  func(r io.Reader) (any, error)
}

// A report represents the results of running an analyzer on the source.
type Report struct{}

var CurlToBashAnalyzer = Analyzer{
	Name: "no-curl-to-shell",
	Doc: `Checks for usage of insecure ` + "`" + `curl install.sh | bash -c` + "`" + ` like patterns.

Supports the following sources:

- aria2c
- curl
- httpie
- wget
- xh

Supports the following sinks:

- ash
- bash
- dash
- sh
- zash
`,
	Run: func(r io.Reader) (any, error) {
		f, err := syntax.NewParser().Parse(r, "")
		if err != nil {
			return nil, fmt.Errorf("parsing shell script: %w", err)
		}

		var found bool
		syntax.Walk(f, func(node syntax.Node) bool {
			binCmd, ok := node.(*syntax.BinaryCmd)
			if !ok {
				return true
			}

			if binCmd.Op != syntax.Pipe && binCmd.Op != syntax.PipeAll {
				return true
			}

			xCmd, ok := binCmd.X.Cmd.(*syntax.CallExpr)
			if !ok {
				return true
			}

			if len(xCmd.Args) == 0 {
				return true
			}

			yCmd, ok := binCmd.Y.Cmd.(*syntax.CallExpr)
			if !ok {
				return true
			}

			if len(yCmd.Args) == 0 {
				return true
			}

			if isDownloadCmd(xCmd.Args[0]) && isShellCmd(yCmd.Args[0]) {
				found = true
			}
			return true
		})

		return found, err
	},
}

func isDownloadCmd(arg *syntax.Word) bool {
	if len(arg.Parts) == 0 {
		return false
	}

	part := arg.Parts[0]
	lit, ok := part.(*syntax.Lit)
	if !ok {
		return false
	}

	switch filepath.Base(lit.Value) {
	case "curl", "wget", "aria2c", "httpie", "xh":
		return true
	default:
		return false
	}
}

func isShellCmd(arg *syntax.Word) bool {
	if len(arg.Parts) == 0 {
		return false
	}

	part := arg.Parts[0]
	word, ok := part.(*syntax.Lit)
	if !ok {
		return false
	}

	switch filepath.Base(word.Value) {
	case "ash", "bash", "dash", "sh", "zsh":
		return true
	default:
		return false
	}
}
