package convert

import (
	"fmt"
	"slices"
	"strings"
	"time"

	cdx "github.com/CycloneDX/cyclonedx-go"

	"gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning/internal/parse"
)

// IntoBOM takes a CI config file and converts it into a valid CycloneDX artifact.
func IntoBOM(config parse.Config) *cdx.BOM {
	now := time.Now().UTC()
	metadata := cdx.Metadata{
		Timestamp: now.String(),
	}

	components := make([]cdx.Component, 0, len(config.Jobs)*2)
	dependencies := make([]cdx.Dependency, 0, len(config.Jobs)*2)

	for _, job := range config.Jobs {
		imgName, imgVersion, found := strings.Cut(job.Image.Name, ":")
		if !found {
			imgVersion = "latest"
		}

		imagePURL := fmt.Sprintf("pkg:docker/%s@%s", imgName, imgVersion)
		imageIndex := slices.IndexFunc(components, func(component cdx.Component) bool {
			return component.BOMRef == imagePURL
		})

		if imageIndex == -1 {
			components = append(components, cdx.Component{
				BOMRef:     imagePURL,
				PackageURL: imagePURL,
				Name:       imgName,
				Version:    imgVersion,
				Type:       cdx.ComponentTypeContainer,
				Properties: &[]cdx.Property{
					{Name: "gitlab:ci:job:name", Value: job.Name},
				},
			})
		} else {
			*components[imageIndex].Properties = append(*components[imageIndex].Properties, cdx.Property{
				Name: "gitlab:ci:job:name", Value: job.Name,
			})
		}

		for _, service := range job.Services {
			serviceName, serviceVersion, found := strings.Cut(service.Name, ":")
			if !found {
				serviceVersion = "latest"
			}

			servicePURL := fmt.Sprintf("pkg:docker/%s@%s", serviceName, serviceVersion)
			serviceIndex := slices.IndexFunc(components, func(component cdx.Component) bool {
				return component.BOMRef == servicePURL
			})

			if serviceIndex == -1 {
				components = append(components, cdx.Component{
					BOMRef:     servicePURL,
					PackageURL: servicePURL,
					Name:       serviceName,
					Version:    serviceVersion,
					Type:       cdx.ComponentTypeContainer,
					Properties: &[]cdx.Property{
						{Name: "gitlab:ci:job:name", Value: job.Name},
					},
				})
				dependencies = append(dependencies,
					cdx.Dependency{
						Ref: servicePURL,
					},
				)
			} else {
				*components[serviceIndex].Properties = append(*components[serviceIndex].Properties, cdx.Property{
					Name: "gitlab:ci:job:name", Value: job.Name,
				})
			}

			if imageIndex == -1 {
				dependencies = append(dependencies,
					cdx.Dependency{
						Ref: imagePURL,
						Dependencies: &[]string{
							servicePURL,
						},
					},
				)
			}
		}
	}

	bom := cdx.NewBOM()
	bom.Metadata = &metadata
	bom.Components = &components
	bom.Dependencies = &dependencies

	return bom
}
