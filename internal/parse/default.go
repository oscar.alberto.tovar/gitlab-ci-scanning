package parse

import (
	"os"
	"slices"
)

type Default struct {
	Image        Image     `yaml:"image" mapstructure:"image"`
	Services     []Service `yaml:"services" mapstructure:"services"`
	AfterScript  []string  `yaml:"after_script" mapstructure:"after_script"`
	BeforeScript []string  `yaml:"before_script" mapstructure:"before_script"`
	Script       []string  `yaml:"script" mapstructure:"script"`
}

func (d *Default) expand(vars map[string]string) {
	for i := 0; i < maxExpansionCount; i++ {
		if !envVarRegexp.MatchString(d.Image.Name) {
			break
		}

		d.Image.Name = os.Expand(d.Image.Name, func(placeholder string) string {
			if slices.Contains(ciVars, placeholder) {
				return "$" + placeholder
			}
			return vars[placeholder]
		})
	}

	// TODO: optimize this to avoid the O(M*N) call here.
	// If M is the number of services, and N is the max
	// expansion count then we have a max of 3M calls here.
	for i := range d.Services {
		for j := 0; j < maxExpansionCount; j++ {
			if !envVarRegexp.MatchString(d.Services[i].Name) {
				break
			}

			d.Services[i].Name = os.Expand(d.Services[i].Name, func(placeholder string) string {
				if slices.Contains(ciVars, placeholder) {
					return "$" + placeholder
				}
				return vars[placeholder]
			})
		}
	}
}
