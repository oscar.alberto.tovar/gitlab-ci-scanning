package parse

import (
	"fmt"
	"io"

	"github.com/mitchellh/mapstructure"
	"gopkg.in/yaml.v3"
)

const (
	keywordDefault   = "default"
	keywordInclude   = "include"
	keywordStages    = "stages"
	keywordVariables = "variables"
	keywordWorkflow  = "workflow"
)

var ciVars = []string{
	"CHAT_CHANNEL",
	"CHAT_INPUT",
	"CHAT_USER_ID",

	"CI",
	"CI_API_V4_URL",
	"CI_API_GRAPHQL_URL",
	"CI_BUILDS_DIR",
	"CI_COMMIT_AUTHOR",
	"CI_COMMIT_BEFORE_SHA",
	"CI_COMMIT_BRANCH",
	"CI_COMMIT_DESCRIPTION",
	"CI_COMMIT_MESSAGE",
	"CI_COMMIT_REF_NAME",
	"CI_COMMIT_REF_PROTECTED",
	"CI_COMMIT_REF_SLUG",
	"CI_COMMIT_SHA",
	"CI_COMMIT_SHORT_SHA",
	"CI_COMMIT_TAG",
	"CI_COMMIT_TAG_MESSAGE",
	"CI_COMMIT_TIMESTAMP",
	"CI_COMMIT_TITLE",
	"CI_CONCURRENT_ID",
	"CI_CONCURRENT_PROJECT_ID",
	"CI_CONFIG_PATH",
	"CI_DEBUG_TRACE",
	"CI_DEBUG_SERVICES",
	"CI_DEFAULT_BRANCH",
	"CI_DEPENDENCY_PROXY_DIRECT_GROUP_IMAGE_PREFIX",
	"CI_DEPENDENCY_PROXY_GROUP_IMAGE_PREFIX",
	"CI_DEPENDENCY_PROXY_PASSWORD",
	"CI_DEPENDENCY_PROXY_SERVER",
	"CI_DEPENDENCY_PROXY_USER",
	"CI_DEPLOY_FREEZE",
	"CI_DEPLOY_PASSWORD",
	"CI_DEPLOY_USER",
	"CI_DISPOSABLE_ENVIRONMENT",
	"CI_ENVIRONMENT_NAME",
	"CI_ENVIRONMENT_SLUG",
	"CI_ENVIRONMENT_URL",
	"CI_ENVIRONMENT_ACTION",
	"CI_ENVIRONMENT_TIER",
	"CI_RELEASE_DESCRIPTION",
	"CI_GITLAB_FIPS_MODE",
	"CI_HAS_OPEN_REQUIREMENTS",
	"CI_JOB_ID",
	"CI_JOB_IMAGE",
	"CI_JOB_JWT",
	"CI_JOB_JWT_V1",
	"CI_JOB_JWT_V2",
	"CI_JOB_MANUAL",
	"CI_JOB_NAME",
	"CI_JOB_NAME_SLUG",
	"CI_JOB_STAGE",
	"CI_JOB_STATUS",
	"CI_JOB_TIMEOUT",
	"CI_JOB_TOKEN",
	"CI_JOB_URL",
	"CI_JOB_STARTED_AT",
	"CI_KUBERNETES_ACTIVE",
	"CI_NODE_INDEX",
	"CI_NODE_TOTAL",
	"CI_OPEN_MERGE_REQUESTS",
	"CI_PAGES_DOMAIN",
	"CI_PAGES_URL",
	"CI_PIPELINE_ID",
	"CI_PIPELINE_IID",
	"CI_PIPELINE_SOURCE",
	"CI_PIPELINE_TRIGGERED",
	"CI_TRIGGER_SHORT_TOKEN",
	"CI_PIPELINE_URL",
	"CI_PIPELINE_CREATED_AT",
	"CI_PIPELINE_NAME",
	"CI_PROJECT_DIR",
	"CI_PROJECT_ID",
	"CI_PROJECT_NAME",
	"CI_PROJECT_NAMESPACE",
	"CI_PROJECT_NAMESPACE_ID",
	"CI_PROJECT_PATH_SLUG",
	"CI_PROJECT_PATH",
	"CI_PROJECT_REPOSITORY_LANGUAGES",
	"CI_PROJECT_ROOT_NAMESPACE",
	"CI_PROJECT_TITLE",
	"CI_PROJECT_DESCRIPTION",
	"CI_PROJECT_URL",
	"CI_PROJECT_VISIBILITY",
	"CI_PROJECT_CLASSIFICATION_LABEL",
	"CI_REGISTRY",
	"CI_REGISTRY_IMAGE",
	"CI_REGISTRY_PASSWORD",
	"CI_REGISTRY_USER",
	"CI_REPOSITORY_URL",
	"CI_RUNNER_DESCRIPTION",
	"CI_RUNNER_EXECUTABLE_ARCH",
	"CI_RUNNER_ID",
	"CI_RUNNER_REVISION",
	"CI_RUNNER_SHORT_TOKEN",
	"CI_RUNNER_TAGS",
	"CI_RUNNER_VERSION",
	"CI_SERVER_FQDN",
	"CI_SERVER_HOST",
	"CI_SERVER_NAME",
	"CI_SERVER_PORT",
	"CI_SERVER_PROTOCOL",
	"CI_SERVER_SHELL_SSH_HOST",
	"CI_SERVER_SHELL_SSH_PORT",
	"CI_SERVER_REVISION",
	"CI_SERVER_TLS_CA_FILE",
	"CI_SERVER_TLS_CERT_FILE",
	"CI_SERVER_TLS_KEY_FILE",
	"CI_SERVER_URL",
	"CI_SERVER_VERSION_MAJOR",
	"CI_SERVER_VERSION_MINOR",
	"CI_SERVER_VERSION_PATCH",
	"CI_SERVER_VERSION",
	"CI_SERVER",
	"CI_SHARED_ENVIRONMENT",
	"CI_TEMPLATE_REGISTRY_HOST",

	"GITLAB_CI",
	"GITLAB_FEATURES",
	"GITLAB_USER_EMAIL",
	"GITLAB_USER_ID",
	"GITLAB_USER_LOGIN",
	"GITLAB_USER_NAME",

	"KUBECONFIG",

	"TRIGGER_PAYLOAD",
}

type parseError struct {
	err error
}

func newParseError(err error) parseError {
	return parseError{err}
}

func (e parseError) Error() string {
	return fmt.Sprintf("parsing gitlab ci config: %s", e.err)
}

func ParseConfig(r io.Reader) (Config, error) {
	m := make(map[string]any)
	err := yaml.NewDecoder(r).Decode(&m)
	if err != nil {
		return Config{}, newParseError(err)
	}

	normalizedConfig, err := ymlconv(m)
	if err != nil {
		return Config{}, newParseError(err)
	}

	var config Config
	err = mapstructure.Decode(normalizedConfig, &config)
	if err != nil {
		return Config{}, newParseError(err)
	}

	config.Default.expand(config.Variables)

	for i, job := range config.Jobs {
		if len(job.Image.Name) == 0 {
			config.Jobs[i].Image = config.Default.Image
		}

		if len(job.Services) == 0 {
			config.Jobs[i].Services = config.Default.Services
		}

		// WARNING: Order matters here!
		// We must first merge the vars from the global declaration
		// and then expand the image name to ensure that we
		// correctly expand the variables in the declaration.
		config.Jobs[i].mergeVars(&config.Variables)
		config.Jobs[i].expand()
	}

	return config, nil
}

func ymlconv(m map[string]any) (map[string]any, error) {
	out := make(map[string]any, len(m))
	// It's possible for every entry in the map
	// to be a job, so we preallocate a slice
	// of similar size.
	jobs := make([]map[string]any, 0, len(m))

	for k, v := range m {
		switch k {
		case keywordDefault:
			v, err := jobconv(v)
			if err != nil {
				return nil, err
			}
			out[k] = v
		// TODO: implement global includes mapping
		// These will need to be flattened somehow
		// into an array/slice.
		case keywordInclude:
		// TODO: implement global stages mapping
		case keywordStages:
		// TODO: implement global variables mapping
		case keywordVariables:
			out[k] = v
		case keywordWorkflow:
		// If not a reserved global keyword, then we
		// assume the mapping entry to be a job.
		default:
			job, err := jobconv(v)
			if err != nil {
				return nil, err
			}
			job["name"] = k
			jobs = append(jobs, job)
		}
	}

	out["jobs"] = jobs
	return out, nil
}

func jobconv(v any) (map[string]any, error) {
	switch val := v.(type) {
	case map[string]any:
		if img, ok := val["image"]; ok {
			img, err := imgconv(img)
			if err != nil {
				return nil, err
			}
			val["image"] = img
		}

		if services, ok := val["services"]; ok {
			services, err := srvconv(services)
			if err != nil {
				return nil, err
			}
			val["services"] = services
		}

		return val, nil
	}

	return nil, fmt.Errorf("%T is not valid for job definition", v)
}

// imgconv attempts to convert an any value into
// into the normalized mapstructure representation
// of an image declaration.
func imgconv(v any) (map[string]any, error) {
	switch val := v.(type) {
	case string:
		return map[string]any{
			"name": val,
		}, nil
	case map[string]any:
		return val, nil
	}
	return nil, fmt.Errorf("unsupported image type %T", v)
}

// srvconv attempts to convert an any valud into
// into the normalized mapstructure representation
// of a services declaration.
func srvconv(v any) ([]map[string]any, error) {
	var out []map[string]any
	switch val := v.(type) {
	case []any:
		for _, srv := range val {
			srv, err := imgconv(srv)
			if err != nil {
				return nil, err
			}
			out = append(out, srv)
		}
		return out, nil
	}
	return nil, fmt.Errorf("%T is not valid for services definition", v)
}

type Config struct {
	Default   Default           `yaml:"default" mapstructure:"default"`
	Variables map[string]string `yaml:"variables" mapstructure:"variables"`
	Jobs      []Job             `yaml:"jobs" mapstructure:"jobs"`
}

type Image struct {
	Name       string         `yaml:"name" mapstructure:"name"`
	Entrypoint []any          `yaml:"entrypoint,omitempty" mapstructure:"entrypoint"`
	Docker     map[string]any `yaml:"docker,omitempty" mapstructure:"docker"`
	PullPolicy string         `yaml:"pull_policy,omitempty" mapstructure:"pull_policy"`
}

type Service struct {
	Name       string     `yaml:"name" mapstructure:"name"`
	Alias      string     `yaml:"alias" mapstructure:"alias"`
	Entrypoint []string   `yaml:"entrypoint" mapstructure:"entrypoint"`
	Command    []string   `yaml:"command" mapstructure:"command"`
	Docker     DockerArgs `yaml:"docker" mapstructure:"docker"`
}

type DockerArgs struct {
	Platform string
	User     string
}
