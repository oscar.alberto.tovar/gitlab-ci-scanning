package parse

import (
	"sort"
	"strings"
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestParseCIConfig(t *testing.T) {
	tests := map[string]struct {
		input  *strings.Reader
		expect Config
	}{
		"simple": {
			input: strings.NewReader(`
go:build:
  image: golang:latest
  script:
    - go build -o app cmd/app/main.go
`),
			expect: Config{
				Jobs: []Job{
					{
						Name: "go:build",
						Image: Image{
							Name: "golang:latest",
						},
						Script: []string{
							"go build -o app cmd/app/main.go",
						},
					},
				},
			},
		},
		"complex": {
			input: strings.NewReader(`
default:
  image:
    name: ruby:latest

go:build:
  image: golang:latest
  script:
    - go build -o app cmd/app/main.go

go:test:
  image: golang:latest
  script:
    - go test -v -race ./...

ruby:test:
  script:
    - rspec -fd spec/

postgres:test-migrations:
  image: golang:latest
  services:
    - name: postgres:16
      alias: pg
  before_script:
    - echo "starting migration tests"
  script:
    - make migrate
  after_script:
    - echo "completed migration tests"

go:deploy:
  stage: deploy
  image:
    name: docker:26
  services:
    - name: docker:26-dind
      alias: docker.d
`),
			expect: Config{
				Default: Default{
					Image: Image{
						Name: "ruby:latest",
					},
				},
				Jobs: []Job{
					{
						Name: "go:build",
						Image: Image{
							Name: "golang:latest",
						},
						Script: []string{
							"go build -o app cmd/app/main.go",
						},
					},
					{
						Name: "go:test",
						Image: Image{
							Name: "golang:latest",
						},
						Script: []string{"go test -v -race ./..."},
					},
					{
						Name: "postgres:test-migrations",
						Image: Image{
							Name: "golang:latest",
						},
						Services: []Service{
							{
								Name:  "postgres:16",
								Alias: "pg",
							},
						},
						BeforeScript: []string{`echo "starting migration tests"`},
						Script:       []string{"make migrate"},
						AfterScript:  []string{`echo "completed migration tests"`},
					},
					{
						Name: "go:deploy",
						Image: Image{
							Name: "docker:26",
						},
						Services: []Service{
							{
								Name:  "docker:26-dind",
								Alias: "docker.d",
							},
						},
					},
					{
						Name: "ruby:test",
						Image: Image{
							Name: "ruby:latest",
						},
						Script: []string{"rspec -fd spec/"},
					},
				},
			},
		},
		"uses defaults services": {
			input: strings.NewReader(`
default:
  image: golang:latest
  services:
    - postgres:16

test:integration:
  script:
    - make integration
`),
			expect: Config{
				Default: Default{
					Image: Image{
						Name: "golang:latest",
					},
					Services: []Service{
						{
							Name: "postgres:16",
						},
					},
				},
				Jobs: []Job{
					{
						Name: "test:integration",
						Image: Image{
							Name: "golang:latest",
						},
						Services: []Service{
							{
								Name: "postgres:16",
							},
						},
						Script: []string{"make integration"},
					},
				},
			},
		},
		"expands images and services": {
			input: strings.NewReader(`
variables:
  GOLANG_IMAGE: "golang:latest"
  POSTGRES_IMAGE: "postgres:16"

default:
  services:
    - $POSTGRES_IMAGE

test:integration:
  image: $GOLANG_IMAGE
  script:
    - make integration
`),
			expect: Config{
				Default: Default{
					Services: []Service{
						{
							Name: "postgres:16",
						},
					},
				},
				Jobs: []Job{
					{
						Name: "test:integration",
						Image: Image{
							Name: "golang:latest",
						},
						Services: []Service{
							{
								Name: "postgres:16",
							},
						},
						Variables: map[string]string{
							"GOLANG_IMAGE":   "golang:latest",
							"POSTGRES_IMAGE": "postgres:16",
						},
						Script: []string{"make integration"},
					},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := ParseConfig(tt.input)

			sort.Slice(tt.expect.Jobs, func(i, j int) bool {
				return tt.expect.Jobs[i].Name < tt.expect.Jobs[j].Name
			})

			sort.Slice(got.Jobs, func(i, j int) bool {
				return got.Jobs[i].Name < got.Jobs[j].Name
			})

			assert.NoError(t, err)
			assert.Equal(t, tt.expect.Default, got.Default)
			assert.Equal(t, tt.expect.Jobs, got.Jobs)
		})
	}
}

func TestImageConversion(t *testing.T) {
	tests := map[string]struct {
		input  any
		expect map[string]any
	}{
		"when image is string": {
			input:  "golang:1.22.3",
			expect: map[string]any{"name": "golang:1.22.3"},
		},
		"when image is a mapping": {
			input: map[string]any{
				"name":       "golang:1.22..3",
				"entrypoint": []string{"/bin/bash", "-c"},
				"docker": map[string]any{
					"platform": "amd64",
					"user":     "root",
				},
			},
			expect: map[string]any{
				"name":       "golang:1.22..3",
				"entrypoint": []string{"/bin/bash", "-c"},
				"docker": map[string]any{
					"platform": "amd64",
					"user":     "root",
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := imgconv(tt.input)
			assert.NoError(t, err, "expected no error when converting image")
			assert.Equal(t, tt.expect, got)
		})
	}
}

func TestImageConversion_Error(t *testing.T) {
	tests := map[string]struct {
		input any
	}{
		"when image is a number": {
			input: 1,
		},
		"when image is a float": {
			input: 1.5,
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := imgconv(tt.input)
			assert.Error(t, err)
			assert.Nil(t, got)
		})
	}
}

func TestServiceConversion(t *testing.T) {
	tests := map[string]struct {
		input  any
		expect []map[string]any
	}{
		"when services contains a string": {
			input: []any{"docker:26-dind"},
			expect: []map[string]any{
				{
					"name": "docker:26-dind",
				},
			},
		},
		"when services contains a mapping": {
			input: []any{
				map[string]any{
					"name":       "docker:26-dind",
					"alias":      "docker.d",
					"entrypoint": []string{"/bin/bash", "-c"},
					"command":    []string{"/docker.sh"},
					"docker": map[string]any{
						"platform": "amd64",
						"user":     "root",
					},
				},
			},
			expect: []map[string]any{
				{
					"name":       "docker:26-dind",
					"alias":      "docker.d",
					"entrypoint": []string{"/bin/bash", "-c"},
					"command":    []string{"/docker.sh"},
					"docker": map[string]any{
						"platform": "amd64",
						"user":     "root",
					},
				},
			},
		},
		"when services contains a string and a mapping": {
			input: []any{
				"postgres:16",
				map[string]any{
					"name":       "docker:26-dind",
					"alias":      "docker.d",
					"entrypoint": []string{"/bin/bash", "-c"},
					"command":    []string{"/docker.sh"},
					"docker": map[string]any{
						"platform": "amd64",
						"user":     "root",
					},
				},
			},
			expect: []map[string]any{
				{
					"name": "postgres:16",
				},
				{
					"name":       "docker:26-dind",
					"alias":      "docker.d",
					"entrypoint": []string{"/bin/bash", "-c"},
					"command":    []string{"/docker.sh"},
					"docker": map[string]any{
						"platform": "amd64",
						"user":     "root",
					},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := srvconv(tt.input)
			assert.NoError(t, err)
			assert.Equal(t, tt.expect, got)
		})
	}
}
