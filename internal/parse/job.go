package parse

import (
	"bytes"
	"encoding/json"
	"fmt"
	"io"
	"os"
	"os/exec"
	"regexp"
	"slices"
	"strings"
)

// Used to check if we should continue expanding variables.
var envVarRegexp = regexp.MustCompile(`(?:\$\{[A-Za-z0-9_]+\}|\$[A-Za-z0-9_]+)`)

// Max limit of recursive variable expansion.
const maxExpansionCount = 3

type Job struct {
	Name         string            `yaml:"name" mapstructure:"name"`
	Image        Image             `yaml:"image" mapstructure:"image"`
	Variables    map[string]string `yaml:"variables" mapstructure:"variables"`
	Services     []Service         `yaml:"services" mapstructure:"services"`
	BeforeScript []string          `yaml:"before_script" mapstructure:"before_script"`
	Script       []string          `yaml:"script" mapstructure:"script"`
	AfterScript  []string          `yaml:"after_script" mapstructure:"after_script"`
}

func (job *Job) mergeVars(vars *map[string]string) {
	if vars == nil || len(*vars) == 0 {
		return
	}

	if len(job.Variables) == 0 {
		job.Variables = make(map[string]string)
	}

	for k, v := range *vars {
		if _, ok := job.Variables[k]; !ok {
			job.Variables[k] = v
		}
	}
}

func (job *Job) expand() {
	for i := 0; i < maxExpansionCount; i++ {
		if !envVarRegexp.MatchString(job.Image.Name) {
			break
		}

		job.Image.Name = os.Expand(job.Image.Name, func(placeholder string) string {
			if slices.Contains(ciVars, placeholder) {
				return "$" + placeholder
			}
			return job.Variables[placeholder]
		})
	}

	// TODO: optimize this to avoid the O(M*N) call here.
	// If M is the number of services, and N is the max
	// expansion count then we have a max of 3M calls here.
	for i := range job.Services {
		for j := 0; j < maxExpansionCount; j++ {
			if !envVarRegexp.MatchString(job.Services[i].Name) {
				break
			}

			job.Services[i].Name = os.Expand(job.Services[i].Name, func(placeholder string) string {
				if slices.Contains(ciVars, placeholder) {
					return "$" + placeholder
				}
				return job.Variables[placeholder]
			})
		}
	}
}

type ShellCheckReport struct {
	Source   string             `json:"source,omitempty"`
	Comments []ShellCheckResult `json:"comments,omitempty"`
}

type ShellCheckResult struct {
	Line      int    `json:"line,omitempty"`
	EndLine   int    `json:"endLine,omitempty"`
	Column    int    `json:"column,omitempty"`
	EndColumn int    `json:"endColumn,omitempty"`
	Level     string `json:"level,omitempty"`
	Code      int    `json:"code,omitempty"`
	Message   string `json:"message,omitempty"`
}

// ShellCheck checks the contents of the Job's `before_script`,
// `script` and `after_script` sections using ShellCheck.
// There are some limitations to the script check functionality.
// Primarily, you can have a script that maps to a singular line
// in the shell script domain, but in the YAML format occupies
// more than one line because of the `>-` multi-line string
// functionality. One way to do this would be to have a mapping
// of the YAML node that resolves overflows.
//
// TODO: handle multi-line strings with replaced line endings
// correctly.
func (j *Job) ShellCheck() (*ShellCheckReport, error) {
	readerCount := len(j.BeforeScript) + len(j.Script) + len(j.AfterScript)
	readers := make([]io.Reader, 0, readerCount)

	for i := range j.BeforeScript {
		readers = append(readers, strings.NewReader(j.BeforeScript[i]+"\n"))
	}

	for i := range j.Script {
		readers = append(readers, strings.NewReader(j.Script[i]+"\n"))
	}

	for i := range j.AfterScript {
		readers = append(readers, strings.NewReader(j.AfterScript[i]+"\n"))
	}

	r := io.MultiReader(readers...)
	stdout, stderr, err := runShellCheck(r)
	if err != nil {
		return nil, fmt.Errorf("validating %s scripts with shellcheck %s: %w", j.Name, &stderr, err)
	}

	var report ShellCheckReport
	report.Source = j.Name
	dec := json.NewDecoder(&stdout)
	for dec.More() {
		if err := dec.Decode(&report); err != nil {
			return nil, fmt.Errorf("decoding shellcheck report: %w", err)
		}
	}
	return &report, nil
}

func runShellCheck(stdin io.Reader) (stdout, stderr bytes.Buffer, err error) {
	cmd := exec.Command("shellcheck", "--color=never", "-f", "json1", "-s", "sh", "-")
	cmd.Stdin = stdin
	cmd.Stdout = &stdout
	cmd.Stderr = &stderr
	err = cmd.Run()
	if err != nil && stderr.Len() != 0 {
		err = fmt.Errorf("running %s: %w", cmd, err)
	} else {
		err = nil
	}
	return
}
