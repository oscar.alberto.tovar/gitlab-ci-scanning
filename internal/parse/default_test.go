package parse

import (
	"testing"

	"github.com/stretchr/testify/assert"
)

func TestExpandDefaultImage(t *testing.T) {
	tests := map[string]struct {
		input     Default
		variables map[string]string
		expect    string
	}{
		"first order expansion": {
			input: Default{
				Image: Image{
					Name: "$IMAGE:$TAG",
				},
			},
			variables: map[string]string{
				"IMAGE": "golang",
				"TAG":   "latest",
			},
			expect: "golang:latest",
		},
		"second order expansion": {
			input: Default{
				Image: Image{
					Name: "$IMAGE",
				},
			},
			variables: map[string]string{
				"IMAGE":      "$IMAGE_NAME:$IMAGE_TAG",
				"IMAGE_NAME": "golang",
				"IMAGE_TAG":  "latest",
			},
			expect: "golang:latest",
		},
		"uses curly braced env var": {
			input: Default{
				Image: Image{
					Name: "${IMAGE}",
				},
			},
			variables: map[string]string{
				"IMAGE": "golang:latest",
			},
			expect: "golang:latest",
		},
		"uses predefined variables": {
			input: Default{
				Image: Image{
					Name: "registry.gitlab.com/group/project/app:$CI_COMMIT_SHORT_SHA",
				},
			},
			variables: map[string]string{
				"CI_COMMIT_SHORT_SHA": "THIS SHOULD NOT APPEAR",
			},
			expect: "registry.gitlab.com/group/project/app:$CI_COMMIT_SHORT_SHA",
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tt.input.expand(tt.variables)
			assert.Equal(t, tt.expect, tt.input.Image.Name)
		})
	}
}

func TestExpandDefaultServices(t *testing.T) {
	input := Default{
		Image: Image{
			Name: "golang:latest",
		},
		Services: []Service{
			{Name: "$POSTGRES_IMAGE"},
			{Name: "$REDIS_IMAGE"},
		},
	}

	variables := map[string]string{
		"POSTGRES_IMAGE": "postgres:16",
		"REDIS_IMAGE":    "redis:7",
	}

	expect := []Service{
		{Name: "postgres:16"},
		{Name: "redis:7"},
	}

	input.expand(variables)

	assert.Equal(t, expect, input.Services)
}
