package parse

import (
	"sort"
	"testing"

	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
)

func TestExpandJobImage(t *testing.T) {
	tests := map[string]struct {
		input  Job
		expect string
	}{
		"first order expansion": {
			input: Job{
				Image: Image{
					Name: "$IMAGE:$TAG",
				},
				Variables: map[string]string{
					"IMAGE": "golang",
					"TAG":   "latest",
				},
			},
			expect: "golang:latest",
		},
		"second order expansion": {
			input: Job{
				Image: Image{
					Name: "$IMAGE",
				},
				Variables: map[string]string{
					"IMAGE":      "$IMAGE_NAME:$IMAGE_TAG",
					"IMAGE_NAME": "golang",
					"IMAGE_TAG":  "latest",
				},
			},
			expect: "golang:latest",
		},
		"uses curly braced env var": {
			input: Job{
				Image: Image{
					Name: "${IMAGE}",
				},
				Variables: map[string]string{
					"IMAGE": "golang:latest",
				},
			},
			expect: "golang:latest",
		},
		"uses predefined variables": {
			input: Job{
				Image: Image{
					Name: "registry.gitlab.com/group/project/app:$CI_COMMIT_SHORT_SHA",
				},
				Variables: map[string]string{
					"CI_COMMIT_SHORT_SHA": "THIS SHOULD NOT APPEAR",
				},
			},
			expect: "registry.gitlab.com/group/project/app:$CI_COMMIT_SHORT_SHA",
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			tt.input.expand()
			assert.Equal(t, tt.expect, tt.input.Image.Name)
		})
	}
}

func TestExpandJobServices(t *testing.T) {
	input := Job{
		Image: Image{
			Name: "golang:latest",
		},
		Services: []Service{
			{Name: "$POSTGRES_IMAGE"},
			{Name: "$REDIS_IMAGE"},
		},
		Variables: map[string]string{
			"POSTGRES_IMAGE": "postgres:16",
			"REDIS_IMAGE":    "redis:7",
		},
	}

	expect := []Service{
		{Name: "postgres:16"},
		{Name: "redis:7"},
	}

	input.expand()

	assert.Equal(t, expect, input.Services)
}

func TestScriptCheck(t *testing.T) {
	t.Skip("Fix flakey tests caused by ShellCheck analysis")

	tests := map[string]struct {
		input  Job
		expect *ShellCheckReport
	}{
		"valid script": {
			input: Job{
				Name:   "go:build",
				Script: []string{"echo 'this is a valid script'"},
			},
			expect: &ShellCheckReport{
				Source:   "go:build",
				Comments: []ShellCheckResult{},
			},
		},
		"invalid before_script": {
			input: Job{
				Name:         "go:build",
				BeforeScript: []string{`echo "unterminated quote`},
			},
			expect: &ShellCheckReport{
				Source: "go:build",
				Comments: []ShellCheckResult{
					{
						Line:      1,
						EndLine:   1,
						Column:    1,
						EndColumn: 1,
						Level:     "info",
						Code:      1009,
						Message:   "The mentioned syntax error was in this simple command.",
					},
					{
						Line:      1,
						EndLine:   1,
						Column:    6,
						EndColumn: 6,
						Level:     "error",
						Code:      1073,
						Message:   "Couldn't parse this double quoted string. Fix to allow more checks.",
					},
					{
						Line:      2,
						EndLine:   2,
						Column:    1,
						EndColumn: 1,
						Level:     "error",
						Code:      1072,
						Message:   "Expected end of double quoted string. Fix any mentioned problems and try again.",
					},
				},
			},
		},
		"invalid before_script script and after_script": {
			input: Job{
				Name:         "go:build",
				BeforeScript: []string{`echo "unterminated quote`},
				Script:       []string{`echo "unterminated quote`},
				AfterScript:  []string{`echo "unterminated quote`},
			},
			expect: &ShellCheckReport{
				Source: "go:build",
				Comments: []ShellCheckResult{
					{
						Line:      3,
						EndLine:   3,
						Column:    1,
						EndColumn: 1,
						Level:     "info",
						Code:      1009,
						Message:   "The mentioned syntax error was in this simple command.",
					},
					{
						Line:      4,
						EndLine:   4,
						Column:    1,
						EndColumn: 1,
						Level:     "error",
						Code:      1072,
						Message:   "Expected end of double quoted string. Fix any mentioned problems and try again.",
					},
					{
						Line:      3,
						EndLine:   3,
						Column:    6,
						EndColumn: 6,
						Level:     "error",
						Code:      1073,
						Message:   "Couldn't parse this double quoted string. Fix to allow more checks.",
					},
					{
						Line:      1,
						EndLine:   1,
						Column:    6,
						EndColumn: 6,
						Level:     "warning",
						Code:      1078,
						Message:   "Did you forget to close this double quoted string?",
					},
					{
						Line:      2,
						EndLine:   2,
						Column:    6,
						EndColumn: 6,
						Level:     "info",
						Code:      1079,
						Message:   "This is actually an end quote, but due to next char it looks suspect.",
					},
				},
			},
		},
	}

	for name, tt := range tests {
		tt := tt
		t.Run(name, func(t *testing.T) {
			t.Parallel()

			got, err := tt.input.ShellCheck()
			require.NoError(t, err, "running shellcheck should not produce an error")

			sort.Slice(got.Comments, func(i, j int) bool {
				a := got.Comments[i]
				b := got.Comments[j]
				return (a.Code + a.Line + a.Column) < (b.Code + b.Line + b.Column)
			})

			sort.Slice(tt.expect.Comments, func(i, j int) bool {
				a := tt.expect.Comments[i]
				b := tt.expect.Comments[j]
				return (a.Code + a.Line + a.Column) < (b.Code + b.Line + b.Column)
			})

			assert.Equal(t, tt.expect, got, "script check results do not match")
		})
	}
}
