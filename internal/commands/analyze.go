package commands

import (
	"fmt"
	"log"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"github.com/urfave/cli/v2"

	"gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning/internal/convert"
	"gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning/internal/parse"
)

func AnalyzeCmd() *cli.Command {
	return &cli.Command{
		Name:  "analyze",
		Usage: "Analyze a .gitlab-ci.yml for build dependencies",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    inputFileFlag,
				EnvVars: inputFileEnvVars,
				Aliases: inputFileAliases,
				Usage:   "Path to .gitlab-ci.yml to analyze - must be merged for maximum accuracy",
				Value:   "-",
			},
			&cli.StringFlag{
				Name:    outputFileFlag,
				EnvVars: outputFileEnvVars,
				Aliases: outputFileAliases,
				Usage:   "Path to output generated CycloneDX SBOM",
				Value:   "-",
			},
		},
		Action: runAnalyze,
	}
}

func runAnalyze(cCtx *cli.Context) error {
	inputFilePath := cCtx.String(inputFileFlag)
	outputFilePath := cCtx.String(outputFileFlag)

	r, err := open(inputFilePath)
	if err != nil {
		return fmt.Errorf("reading config file: %w", err)
	}

	defer func() {
		if err := r.Close(); err != nil {
			log.Printf("Could not close ci input file %q: %s", inputFilePath, err)
		}
	}()

	config, err := parse.ParseConfig(r)
	if err != nil {
		return err
	}

	w, err := create(outputFilePath)
	if err != nil {
		return fmt.Errorf("writing sbom to output file %q: %w", outputFilePath, err)
	}

	defer func() {
		if err := w.Close(); err != nil {
			fmt.Printf("Could not close sbom output file %q: %s", outputFilePath, err)
		}
	}()

	bom := convert.IntoBOM(config)
	enc := cdx.NewBOMEncoder(w, cdx.BOMFileFormatJSON).SetPretty(true)
	if err := enc.Encode(bom); err != nil {
		return fmt.Errorf("encoding cyclonedx sbom to json: %w", err)
	}

	return nil
}
