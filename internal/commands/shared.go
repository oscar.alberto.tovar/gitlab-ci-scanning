package commands

import (
	"os"
	"path/filepath"

	cdx "github.com/CycloneDX/cyclonedx-go"
)

var (
	inputFileFlag    = "input-file"
	inputFileEnvVars = []string{"GITLAB_CI_SCANNING_INPUT_FILE"}
	inputFileAliases = []string{"i"}

	outputFileFlag    = "output-file"
	outputFileEnvVars = []string{"GITLAB_CI_SCANNING_OUTPUT_FILE"}
	outputFileAliases = []string{"o"}
)

// open opens an os.File for reading. It handles the special
// case `-` as a stdin redirection.
func open(path string) (*os.File, error) {
	if path == "-" {
		return os.Stdin, nil
	}

	return os.Open(path)
}

// create creates or truncates an os.File. It handles the special
// case `-` as a stdout redirection.
func create(path string) (*os.File, error) {
	if path == "-" {
		return os.Stdout, nil
	}

	return os.Create(path)
}

func detectBOMExtension(path string) cdx.BOMFileFormat {
	var inputFormat cdx.BOMFileFormat
	switch filepath.Ext(path) {
	case "xml":
		inputFormat = cdx.BOMFileFormatXML
	default:
		inputFormat = cdx.BOMFileFormatJSON
	}

	return inputFormat
}
