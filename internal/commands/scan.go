package commands

import (
	"errors"
	"fmt"
	"slices"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"github.com/urfave/cli/v2"
	"gopkg.in/yaml.v3"
)

func ScanCmd() *cli.Command {
	return &cli.Command{
		Name:  "scan",
		Usage: "Dynamically create container-scanning jobs from a CycloneDX SBOM",
		Flags: []cli.Flag{
			&cli.StringFlag{
				Name:    inputFileFlag,
				EnvVars: inputFileEnvVars,
				Aliases: inputFileAliases,
				Usage:   "Path to CycloneDX SBOM to scan - must contain \"docker\" component(s)",
				Value:   "-",
			},
			&cli.StringFlag{
				Name:    outputFileFlag,
				EnvVars: outputFileEnvVars,
				Aliases: outputFileAliases,
				Usage:   "Path to output generated scan CI/CD scan config",
				Value:   "-",
			},
		},
		Action: runScan,
	}
}

func runScan(cCtx *cli.Context) error {
	inputFilePath := cCtx.String(inputFileFlag)
	outFilePath := cCtx.String(outputFileFlag)

	input, err := open(inputFilePath)
	if err != nil {
		return fmt.Errorf("opening cyclonedx sbom: %w", err)
	}

	bom := cdx.NewBOM()
	inputFormat := detectBOMExtension(inputFilePath)
	dec := cdx.NewBOMDecoder(input, inputFormat)
	if err := dec.Decode(bom); err != nil {
		err = fmt.Errorf("decoding cyclonedx sbom: %w", err)
		fileErr := input.Close()
		return errors.Join(fileErr, err)
	}

	if err := input.Close(); err != nil {
		return fmt.Errorf("closing cyclonedx sbom file: %w", err)
	}

	config, ok := NewScanConfig(bom)
	if !ok {
		return nil
	}

	output, err := create(outFilePath)
	if err != nil {
		return fmt.Errorf("opening dynamic config file: %w", err)
	}

	err = yaml.NewEncoder(output).Encode(&config)
	if err != nil {
		err = fmt.Errorf("marshaling dynamic config to yaml: %w", err)
		fileErr := output.Close()
		return errors.Join(fileErr, err)
	}

	if err := output.Close(); err != nil {
		return fmt.Errorf("closing dynamic config yaml file: %w", err)
	}

	return nil
}

type ScanConfig struct {
	Include []configTemplate `yaml:"include"`
	ScanJob scanJob          `yaml:"container_scanning"`
}

type configTemplate struct {
	Template string `yaml:"template"`
}

type scanJob struct {
	Parallel matrix `yaml:"parallel"`
}

type matrix struct {
	Matrix []scanPermutation
}

type scanPermutation struct {
	CSImage string `yaml:"CS_IMAGE"`
}

// NewScanConfig returns a new dynamic scan configuration that covers
// all the container images in the supplied CycloneDX SBOM.
func NewScanConfig(bom *cdx.BOM) (*ScanConfig, bool) {
	if bom == nil {
		return nil, false
	}

	var cfg ScanConfig
	cfg.Include = []configTemplate{
		{
			Template: "Jobs/Container-Scanning.gitlab-ci.yml",
		},
	}
	permutations := []scanPermutation{}
	for _, component := range *bom.Components {

		image := component.Name + ":" + component.Version

		exists := slices.ContainsFunc(permutations, func(p scanPermutation) bool {
			return p.CSImage == image
		})

		if exists {
			continue
		}

		permutations = append(permutations, scanPermutation{image})
	}

	cfg.ScanJob.Parallel.Matrix = permutations
	return &cfg, true
}
