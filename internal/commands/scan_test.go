package commands_test

import (
	"testing"

	cdx "github.com/CycloneDX/cyclonedx-go"
	"github.com/stretchr/testify/assert"
	"github.com/stretchr/testify/require"
	"gopkg.in/yaml.v3"

	"gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning/internal/commands"
)

func TestConfig(t *testing.T) {
	tests := map[string]struct {
		input  []cdx.Component
		expect string
	}{
		"single job images": {
			input: []cdx.Component{
				{
					Name:    "golang",
					Version: "1.22.0",
				},
			},
			expect: `include:
    - template: Jobs/Container-Scanning.gitlab-ci.yml
container_scanning:
    parallel:
        matrix:
            - CS_IMAGE: golang:1.22.0
`,
		},
		"multiple job images": {
			input: []cdx.Component{
				{
					Name:    "golang",
					Version: "1.20.0",
				},
				{
					Name:    "golang",
					Version: "1.21.0",
				},
				{
					Name:    "golang",
					Version: "1.23.0",
				},
			},
			expect: `include:
    - template: Jobs/Container-Scanning.gitlab-ci.yml
container_scanning:
    parallel:
        matrix:
            - CS_IMAGE: golang:1.20.0
            - CS_IMAGE: golang:1.21.0
            - CS_IMAGE: golang:1.23.0
`,
		},
		"duplicate job images": {
			input: []cdx.Component{
				{
					Name:    "golang",
					Version: "1.20.0",
				},
				{
					Name:    "golang",
					Version: "1.20.0",
				},
				{
					Name:    "golang",
					Version: "1.21.0",
				},
				{
					Name:    "golang",
					Version: "1.23.0",
				},
			},
			expect: `include:
    - template: Jobs/Container-Scanning.gitlab-ci.yml
container_scanning:
    parallel:
        matrix:
            - CS_IMAGE: golang:1.20.0
            - CS_IMAGE: golang:1.21.0
            - CS_IMAGE: golang:1.23.0
`,
		},
	}

	for name, tt := range tests {
		tt := tt

		t.Run(name, func(t *testing.T) {
			t.Parallel()

			bom := cdx.NewBOM()
			bom.Components = &tt.input
			config, ok := commands.NewScanConfig(bom)
			require.True(t, ok, "error not expected with valid input")

			got, err := yaml.Marshal(&config)
			require.NoError(t, err)

			assert.Equalf(t, tt.expect, string(got), "the dynamic scan config did not match")
		})
	}
}
