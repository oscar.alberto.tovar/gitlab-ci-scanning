package main

import (
	"log"
	"os"

	"github.com/urfave/cli/v2"

	"gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning/internal/commands"
)

func main() {
	app := &cli.App{
		Name:    "gitlab-ci-scanning",
		Usage:   "A security tool for your .gitlab-ci.yml build pipelines",
		Authors: []*cli.Author{{Name: "Oscar Alberto Tovar"}},
		Commands: []*cli.Command{
			commands.AnalyzeCmd(),
			commands.ScanCmd(),
		},
	}

	if err := app.Run(os.Args); err != nil {
		log.Fatalf("%s", err)
	}
}
