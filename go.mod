module gitlab.com/oscar.alberto.tovar/gitlab-ci-scanning

go 1.22.3

require (
	github.com/CycloneDX/cyclonedx-go v0.8.0
	github.com/mitchellh/mapstructure v1.5.0
	github.com/stretchr/testify v1.9.0
	github.com/urfave/cli/v2 v2.27.2
	gopkg.in/yaml.v3 v3.0.1
	mvdan.cc/sh/v3 v3.8.0
)

require (
	github.com/cpuguy83/go-md2man/v2 v2.0.4 // indirect
	github.com/davecgh/go-spew v1.1.1 // indirect
	github.com/pmezard/go-difflib v1.0.0 // indirect
	github.com/russross/blackfriday/v2 v2.1.0 // indirect
	github.com/xrash/smetrics v0.0.0-20240312152122-5f08fbb34913 // indirect
)
